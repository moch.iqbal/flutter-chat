## How to run

- Clone this repo
- Run `flutter pub get`
- Run `flutter run` (remember open simulator or connect physical device, iOS auto run additional command `pod install`)
